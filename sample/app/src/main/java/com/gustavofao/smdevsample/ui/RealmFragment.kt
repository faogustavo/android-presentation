package com.gustavofao.smdevsample.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gustavofao.smdevsample.dao.RepoDao
import com.gustavofao.smdevsample.dao.UserDao
import com.gustavofao.smdevsample.databinding.RealmFragmentBinding
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject

class RealmFragment : Fragment() {

    private lateinit var binding: RealmFragmentBinding

    val userDao: UserDao by inject()
    val repoDao: RepoDao = get()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return RealmFragmentBinding.inflate(inflater, container, false).apply {
            binding = this
        }.root
    }

}