package com.gustavofao.smdevsample.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.gustavofao.smdevsample.service.GithubService
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val ServiceModule = module {
    single<Gson> {
        GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create()
    }
    single<Retrofit> {
        Retrofit.Builder()
            .baseUrl("https://api.github.com")
            .addConverterFactory(GsonConverterFactory.create(get()))
            .build()
    }
    single<GithubService> {
        val retrofit: Retrofit = get()
        retrofit.create(GithubService::class.java)
    }
}