package com.gustavofao.smdevsample.toolkit

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData

// Based on https://github.com/googlesamples/android-architecture-components
abstract class NetworkBoundResource<DBDataType, APIDataType> (private val appExecutors: AppExecutors) {

    private val result = MediatorLiveData<Resource<DBDataType>>()

    init {
        result.postValue(Resource.isLoading())
        val dbSource = loadFromDb()
        result.addSource(dbSource) {
            result.removeSource(dbSource)
            if (shouldFetchFromNetwork(it)) {
                fetchFromNetwork(dbSource)
            } else {
                result.postValue(Resource.success(it))
            }
        }
    }

    private fun fetchFromNetwork(dbSource: LiveData<DBDataType>) {
        result.addSource(dbSource) { dbData ->
            result.removeSource(dbSource)
            result.postValue(Resource.isLoading(dbData))

            appExecutors.networkIO {
                val request = executeRequest()
                when (request.status) {
                    APIResource.Status.SUCCESS -> {
                        appExecutors.diskIO {
                            onResultSuccess(request.data, dbData)
                            appExecutors.mainThread {
                                result.addSource(loadFromDb()) { finalValue ->
                                    result.postValue(Resource.success(finalValue))
                                }
                            }
                        }
                    }
                    APIResource.Status.ERROR -> {
                        appExecutors.mainThread {
                            result.addSource(loadFromDb()) { finalValue ->
                                result.postValue(Resource.error(request.message, finalValue))
                            }
                        }
                    }
                }
            }
        }
    }

    abstract fun executeRequest(): APIResource<APIDataType>

    abstract fun loadFromDb(): LiveData<DBDataType>

    abstract fun onResultSuccess(response: APIDataType?, dbData: DBDataType?)

    fun shouldFetchFromNetwork(data: DBDataType?) = true

    fun asLiveData() = result as LiveData<Resource<DBDataType>>

}