package com.gustavofao.smdevsample.vm

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.gustavofao.smdevsample.entity.RepoEntity
import com.gustavofao.smdevsample.entity.UserEntity
import com.gustavofao.smdevsample.repo.github.GithubRepository
import com.gustavofao.smdevsample.service.response.RepoResponse
import com.gustavofao.smdevsample.toolkit.Resource

class GithubViewModel(private val githubRepository: GithubRepository) : ViewModel() {

    private val _username: MutableLiveData<String> = MutableLiveData()
    private val _tryAgain: MutableLiveData<Int> = MutableLiveData<Int>().apply { value = 0 }

    private val _user: LiveData<Resource<UserEntity>> =
        Transformations.switchMap(_username) { username ->
            Transformations.switchMap(_tryAgain) {
                githubRepository.getUser(username)
            }
        }

    private val _repositories: LiveData<Resource<List<RepoEntity>>> =
        Transformations.switchMap(_user) { user ->
            Transformations.switchMap(_tryAgain) {
                if (user.isSuccess()) {
                    user.data?.let {
                        githubRepository.getRepos(it.username)
                    } ?: MutableLiveData()
                } else {
                    MutableLiveData()
                }
            }
        }

    val user: LiveData<Resource<UserEntity>>
        get() = _user

    val repositories: LiveData<Resource<List<RepoEntity>>>
        get() = _repositories

    fun tryAgain() {
        _tryAgain.postValue((_tryAgain.value ?: 0).inc())
    }

    fun setUsername(username: String) {
        _username.postValue(username)
    }
}