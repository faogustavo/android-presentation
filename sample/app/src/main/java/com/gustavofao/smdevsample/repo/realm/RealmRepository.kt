package com.gustavofao.smdevsample.repo.realm

import com.gustavofao.smdevsample.model.Dog
import com.gustavofao.smdevsample.model.Person
import io.realm.RealmResults

interface RealmRepository {

    fun getAllPeople(): RealmResults<Person>

    fun getPerson(uuid: String): Person?

    fun savePerson(person: Person)

    fun deletePerson(uuid: String): Boolean

    fun getPuppies(): RealmResults<Dog>

}