package com.gustavofao.smdevsample.service.response

import com.google.gson.annotations.SerializedName
import com.gustavofao.smdevsample.entity.RepoEntity

data class RepoResponse(
    val id: Long,
    val name: String,
    @SerializedName("full_name")
    val fullName: String,
    val description: String?,
    @SerializedName("fork")
    val isFork: Boolean,
    val owner: UserResponse
)

fun RepoResponse.toRepoEntity() = RepoEntity(
    id, name, fullName, description, isFork, owner.id
)