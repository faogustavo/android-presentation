package com.gustavofao.smdevsample.di

import com.gustavofao.smdevsample.repo.github.AppGithubRepository
import com.gustavofao.smdevsample.repo.github.GithubRepository
import com.gustavofao.smdevsample.repo.realm.AppRealmRepository
import com.gustavofao.smdevsample.repo.realm.RealmRepository
import org.koin.dsl.module.module

val RepositoryModule = module {
    single<RealmRepository> { AppRealmRepository(get()) }
    single<GithubRepository> { AppGithubRepository(get(), get(), get(), get()) }
}