package com.gustavofao.smdevsample.di

import com.gustavofao.smdevsample.toolkit.AppExecutors
import org.koin.dsl.module.module

val ApplicationModule = module {
    single<AppExecutors> { AppExecutors() }
}