package com.gustavofao.smdevsample.model;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import com.gustavofao.smdevsample.entity.RepoEntity;
import com.gustavofao.smdevsample.entity.UserEntity;

import java.util.List;

public class UserModel {

    @Embedded
    public UserEntity user;

    @Relation(
        entity = RepoEntity.class,
        entityColumn = "ownerId",
        parentColumn = "id"
    )
    public List<RepoEntity> repositories;

}
