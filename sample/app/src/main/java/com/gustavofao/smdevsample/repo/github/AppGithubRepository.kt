package com.gustavofao.smdevsample.repo.github

import android.arch.lifecycle.LiveData
import com.gustavofao.smdevsample.dao.RepoDao
import com.gustavofao.smdevsample.dao.UserDao
import com.gustavofao.smdevsample.entity.RepoEntity
import com.gustavofao.smdevsample.entity.UserEntity
import com.gustavofao.smdevsample.service.GithubService
import com.gustavofao.smdevsample.service.response.RepoResponse
import com.gustavofao.smdevsample.service.response.UserResponse
import com.gustavofao.smdevsample.service.response.toRepoEntity
import com.gustavofao.smdevsample.service.response.toUserEntity
import com.gustavofao.smdevsample.toolkit.APIResource
import com.gustavofao.smdevsample.toolkit.AppExecutors
import com.gustavofao.smdevsample.toolkit.NetworkBoundResource
import com.gustavofao.smdevsample.toolkit.Resource

class AppGithubRepository(
    private val appExecutors: AppExecutors,
    private val githubService: GithubService,
    private val userDao: UserDao,
    private val repoDao: RepoDao
) : GithubRepository {
    override fun getUser(username: String): LiveData<Resource<UserEntity>> {
        return object: NetworkBoundResource<UserEntity, UserResponse>(appExecutors){
            override fun loadFromDb() = userDao.getByUsername(username)

            override fun executeRequest() = APIResource.execute(githubService.getUser(username))

            override fun onResultSuccess(response: UserResponse?, dbData: UserEntity?) {
                dbData?.let { userDao.delete(it) }
                response?.let { userDao.add(it.toUserEntity()) }
            }
        }.asLiveData()
    }

    override fun getRepos(username: String): LiveData<Resource<List<RepoEntity>>> {
        return object : NetworkBoundResource<List<RepoEntity>, List<RepoResponse>>(appExecutors) {
            override fun loadFromDb() =
                    repoDao.getAll(username)

            override fun executeRequest() =
                    APIResource.execute(githubService.getRepos(username))

            override fun onResultSuccess(response: List<RepoResponse>?, dbData: List<RepoEntity>?) {
                dbData?.let { repoDao.delete(it) }
                response?.let { repoDao.addAll(it.map { it.toRepoEntity() }) }
            }
        }.asLiveData()
    }
}
