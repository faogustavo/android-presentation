package com.gustavofao.smdevsample.toolkit

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.gustavofao.smdevsample.dao.RepoDao
import com.gustavofao.smdevsample.dao.UserDao
import com.gustavofao.smdevsample.entity.RepoEntity
import com.gustavofao.smdevsample.entity.UserEntity

@Database(
    entities = arrayOf(
        UserEntity::class,
        RepoEntity::class
    ),
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getUserDao(): UserDao

    abstract fun getRepoDao(): RepoDao

}