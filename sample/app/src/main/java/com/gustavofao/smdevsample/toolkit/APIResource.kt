package com.gustavofao.smdevsample.toolkit

import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

// Based on https://github.com/googlesamples/android-architecture-components
class APIResource<T>(val status: Status, val data: T? = null, val message: String? = null) {
    enum class Status {
        ERROR,
        SUCCESS
    }

    companion object {
        fun <T> execute(call: Call<T>) = APIResource.create(call.execute())

        fun <T> create(error: Throwable) = APIResource<T>(Status.ERROR, message = error.message
                ?: "unknow error")

        fun <T> create(response: Response<T>): APIResource<T> {
            return if (response.isSuccessful) {
                val body = response.body()
                if (body == null || response.code() == 204) {
                    APIResource(Status.ERROR)
                } else {
                    APIResource(Status.ERROR, body)
                }
            } else {
                val msg = response.errorBody()?.string()
                val errorMsg = if (msg.isNullOrEmpty()) {
                    response.message()
                } else {
                    val json = JSONObject(msg)
                    if (json.has("message")) {
                        json.getString("message")
                    } else {
                        "unknow error"
                    }
                }
                APIResource(Status.ERROR, message = errorMsg)
            }
        }
    }

    fun isSuccessful() = status in arrayOf(Status.SUCCESS)

    fun isError() = status in arrayOf(Status.ERROR)
}