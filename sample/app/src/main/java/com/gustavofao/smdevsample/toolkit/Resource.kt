package com.gustavofao.smdevsample.toolkit

class Resource<T>(val data: T?, val errorMessage: String?, val status: Status) {
    enum class Status {
        LOADING,
        SUCCESS,
        ERROR
    }

    companion object {
        fun <T> isLoading(data: T? = null) =
                Resource<T>(data, null, Status.LOADING)

        fun <T> success(data: T? = null) =
                Resource<T>(data, null, Status.SUCCESS)

        fun <T> error(message: String? = null, data: T? = null) =
                Resource<T>(null, message, Status.ERROR)
    }

    fun isSuccess() = status == Status.SUCCESS

    fun isError() = status == Status.ERROR

    fun isLoading() = status == Status.LOADING
}