package com.gustavofao.smdevsample.ui

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gustavofao.smdevsample.R
import com.gustavofao.smdevsample.databinding.GithubFragmentBinding
import com.gustavofao.smdevsample.toolkit.Extensions.hide
import com.gustavofao.smdevsample.toolkit.Extensions.loadImage
import com.gustavofao.smdevsample.toolkit.Extensions.show
import com.gustavofao.smdevsample.toolkit.Extensions.year
import com.gustavofao.smdevsample.toolkit.Resource
import com.gustavofao.smdevsample.ui.adapters.GithubRepositoryAdapter
import com.gustavofao.smdevsample.vm.GithubViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class GithubFragment : Fragment() {

    private val githubViewModel: GithubViewModel by viewModel()

    private lateinit var binding: GithubFragmentBinding
    private lateinit var adapter: GithubRepositoryAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return GithubFragmentBinding.inflate(inflater, container, false).apply {
            binding = this
            adapter = GithubRepositoryAdapter()

            repositoryList.adapter = adapter
            repositoryList.layoutManager = LinearLayoutManager(context)

            tryAgainButton.setOnClickListener {
                githubViewModel.tryAgain()
            }
        }.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        githubViewModel.setUsername("faogustavo")
        githubViewModel.user.observe(this, Observer {
            when (it?.status) {
                Resource.Status.LOADING -> {
                    binding.fullscreenProgress.show()
                    binding.fullscreenError.hide()
                }
                Resource.Status.ERROR -> {
                    binding.fullscreenError.show()
                    binding.fullscreenProgress.hide()
                }
                Resource.Status.SUCCESS -> {
                    it.data?.let { user ->
                        binding.profileImage.loadImage(user.avatarUrl)
                        binding.profileName.text = user.name ?: user.username
                        binding.memberSince.text = context?.getString(R.string.member_since, user.createdAt?.year())
                    }
                    binding.fullscreenProgress.hide()
                }
            }
        })
        githubViewModel.repositories.observe(this, Observer {
            binding.swipeRefreshLayout.isRefreshing = it?.isLoading() ?: false
            when (it?.status) {
                Resource.Status.ERROR -> {
                    binding.fullscreenError.show()
                    binding.fullscreenProgress.hide()
                }
                Resource.Status.SUCCESS -> {
                    binding.fullscreenError.hide()
                    adapter.setData(it.data ?: emptyList())
                }
            }
        })
    }

}