package com.gustavofao.smdevsample.entity

import android.arch.persistence.room.Entity
import java.util.Date

@Entity(
    tableName = "user",
    primaryKeys = arrayOf("id")
)
data class UserEntity(
    val id: Long,
    val username: String,
    val avatarUrl: String,
    val name: String?,
    val company: String?,
    val location: String?,
    val createdAt: Date?
)