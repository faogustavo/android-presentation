package com.gustavofao.smdevsample.toolkit.Extensions

import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.squareup.picasso.Picasso

fun ImageView.loadImage(
    src: String,
    @DrawableRes
    placeHolder: Int? = null,
    @DrawableRes
    error: Int? = null
) {
    var picasso = Picasso.get()
            .load(src)

    placeHolder?.let {
        picasso = picasso.placeholder(it)
    }

    error?.let {
        picasso = picasso.error(it)
    }

    picasso.into(this)
}