package com.gustavofao.smdevsample.repo.realm

import com.gustavofao.smdevsample.model.Dog
import com.gustavofao.smdevsample.model.Person
import com.gustavofao.smdevsample.toolkit.Extensions.findAll
import com.gustavofao.smdevsample.toolkit.Extensions.findFirst
import com.gustavofao.smdevsample.toolkit.Extensions.onTransaction
import io.realm.Realm

class AppRealmRepository(private val realm: Realm) : RealmRepository {

    override fun getAllPeople() = realm.findAll(Person::class.java)

    override fun getPerson(uuid: String) = realm.findFirst(Person::class.java, "uuid", uuid)

    override fun savePerson(person: Person) {
        realm.onTransaction {
            copyToRealmOrUpdate(person)
        }
    }

    override fun deletePerson(uuid: String): Boolean {
        realm.where(Person::class.java)
                .equalTo("uuid", uuid)
                .findFirst()?.let {
                    realm.onTransaction {
                        it.deleteFromRealm()
                    }
                    return true
                }
        return false
    }

    override fun getPuppies() = realm.where(Dog::class.java).findAll()

}