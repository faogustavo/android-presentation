package com.gustavofao.smdevsample.di

import io.realm.Realm
import org.koin.dsl.module.module

val RealmModule = module {
    single<Realm> {
        Realm.init(get())
        Realm.getDefaultInstance()
    }
}