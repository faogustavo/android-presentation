package com.gustavofao.smdevsample.service.response

import com.google.gson.annotations.SerializedName
import com.gustavofao.smdevsample.entity.UserEntity
import java.util.*

data class UserResponse(
    val id: Long,
    val username: String,
    @SerializedName("avatar_url")
    val avatarUrl: String,
    val name: String?,
    val company: String?,
    val location: String?,
    @SerializedName("created_at")
    val createdAt: Date?
)

fun UserResponse.toUserEntity() = UserEntity(
    id, username, avatarUrl, name, company, location, createdAt
)