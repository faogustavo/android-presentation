package com.gustavofao.smdevsample.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gustavofao.smdevsample.databinding.GithubRepositoryBinding
import com.gustavofao.smdevsample.entity.RepoEntity

class GithubRepositoryAdapter : RecyclerView.Adapter<GithubRepositoryAdapter.GithubRepositoryViewHolder>() {

    private var data: List<RepoEntity> = emptyList()

    fun setData(newData: List<RepoEntity>) {
        this.data = newData
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            GithubRepositoryViewHolder(GithubRepositoryBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: GithubRepositoryViewHolder, position: Int) {
        holder.bind(data[position])
    }

    class GithubRepositoryViewHolder(val binding: GithubRepositoryBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(repo: RepoEntity) {
            binding.repoTitle.text = repo.fullName
            binding.repoDescription.text = repo.description
            binding.forkIcon.visibility = if (repo.isFork) View.VISIBLE else View.GONE
        }
    }

}