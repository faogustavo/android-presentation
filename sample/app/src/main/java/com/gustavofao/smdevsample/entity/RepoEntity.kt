package com.gustavofao.smdevsample.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey

@Entity(
    tableName = "repository",
    primaryKeys = arrayOf("id"),
    foreignKeys = arrayOf(
        ForeignKey(
            entity = UserEntity::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("ownerId"),
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        )
    )
)
data class RepoEntity(
    val id: Long,
    val name: String,
    val fullName: String,
    val description: String?,
    val isFork: Boolean,
    val ownerId: Long
)