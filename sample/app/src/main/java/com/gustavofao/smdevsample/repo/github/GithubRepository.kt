package com.gustavofao.smdevsample.repo.github

import android.arch.lifecycle.LiveData
import com.gustavofao.smdevsample.entity.RepoEntity
import com.gustavofao.smdevsample.entity.UserEntity
import com.gustavofao.smdevsample.toolkit.Resource

interface GithubRepository {

    fun getUser(username: String): LiveData<Resource<UserEntity>>

    fun getRepos(username: String): LiveData<Resource<List<RepoEntity>>>

}