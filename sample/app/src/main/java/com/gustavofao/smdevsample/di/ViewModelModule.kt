package com.gustavofao.smdevsample.di

import com.gustavofao.smdevsample.vm.GithubViewModel
import com.gustavofao.smdevsample.vm.RealmViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val ViewModelModule = module {
    viewModel<RealmViewModel> { RealmViewModel(get()) }
    viewModel<GithubViewModel> { GithubViewModel(get()) }
}