package com.gustavofao.smdevsample

import android.app.Application
import com.gustavofao.smdevsample.di.*
import org.koin.android.ext.android.startKoin

class App : Application() {

    val koinModules by lazy {
        listOf(
            ApplicationModule,
            ServiceModule,
            RealmModule,
            DatabaseModule,
            RepositoryModule,
            ViewModelModule
        )
    }

    override fun onCreate() {
        super.onCreate()
        startKoin(this, koinModules)
    }

}