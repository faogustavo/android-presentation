package com.gustavofao.smdevsample.toolkit.Extensions

import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmResults

fun Realm.onTransaction(block: Realm.() -> Unit) {
    beginTransaction()
    block()
    commitTransaction()
}

fun <T : RealmModel> Realm.findAll(clazz: Class<T>, orderBy: String? = null, limit: Long? = null): RealmResults<T> {
    var query = where(clazz)

    orderBy?.let {
        query = query.sort(it)
    }

    limit?.let {
        query = query.limit(it)
    }

    return query.findAll()
}

fun <T : RealmModel> Realm.findFirst(clazz: Class<T>, fieldName: String? = null, fieldValue: String? = null): T? {
    var query = where(clazz)

    fieldName?.let {
        query = query.equalTo(fieldName, fieldValue)
    }

    return query.findFirst()
}
