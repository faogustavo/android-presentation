package com.gustavofao.smdevsample.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.gustavofao.smdevsample.entity.RepoEntity

@Dao
interface RepoDao {

    @Query("SELECT * FROM repository INNER JOIN user ON user.id = repository.ownerId WHERE user.username = :username")
    fun getAll(username: String): LiveData<List<RepoEntity>>

    @Insert
    fun add(repo: RepoEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addAll(repos: List<RepoEntity>)

    @Delete
    fun delete(repos: List<RepoEntity>)

}