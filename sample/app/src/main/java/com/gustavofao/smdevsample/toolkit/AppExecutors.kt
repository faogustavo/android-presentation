package com.gustavofao.smdevsample.toolkit

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors

// Based on https://github.com/googlesamples/android-architecture-components
class AppExecutors(
        private val diskIO: Executor = Executors.newSingleThreadExecutor(),
        private val networkIO: Executor = Executors.newFixedThreadPool(3),
        private val mainThread: Executor = MainThreadExecutor()
) {

    fun diskIO(block: () -> Unit) {
        diskIO.execute(block)
    }

    fun networkIO(block: () -> Unit) {
        networkIO.execute(block)
    }

    fun mainThread(block: () -> Unit) {
        mainThread.execute {
            block()
        }
    }

    private class MainThreadExecutor : Executor {
        private val mainThreadHandler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable) {
            mainThreadHandler.post(command)
        }
    }
}