package com.gustavofao.smdevsample.di

import android.arch.persistence.room.Room
import com.gustavofao.smdevsample.dao.RepoDao
import com.gustavofao.smdevsample.dao.UserDao
import com.gustavofao.smdevsample.toolkit.AppDatabase
import org.koin.dsl.module.module

val DatabaseModule = module {
    single<AppDatabase> {
        Room.databaseBuilder(get(), AppDatabase::class.java, "app-database").build()
    }
    single<UserDao> {
        val database: AppDatabase = get()
        database.getUserDao()
    }
    single<RepoDao> {
        val database: AppDatabase = get()
        database.getRepoDao()
    }
}