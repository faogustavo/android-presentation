package com.gustavofao.smdevsample.vm

import android.arch.lifecycle.ViewModel
import com.gustavofao.smdevsample.repo.realm.RealmRepository

class RealmViewModel(val realmRepository: RealmRepository) : ViewModel() {

}