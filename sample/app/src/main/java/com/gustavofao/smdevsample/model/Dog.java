package com.gustavofao.smdevsample.model;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;

public class Dog extends RealmObject {

    @PrimaryKey
    private String uuid = UUID.randomUUID().toString();

    private int age;
    private String name;
    private String color;

    @LinkingObjects("dogs")
    private final RealmResults<Person> owners = null;

    public Dog() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public RealmResults<Person> getOwner() {
        return owners;
    }
}