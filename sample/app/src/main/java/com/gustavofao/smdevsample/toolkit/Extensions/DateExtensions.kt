package com.gustavofao.smdevsample.toolkit.Extensions

import java.util.*

fun Date.year(): Int = Calendar.getInstance().also {
    it.time = this
}.get(Calendar.YEAR)