package com.gustavofao.smdevsample.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.gustavofao.smdevsample.entity.UserEntity
import com.gustavofao.smdevsample.model.UserModel

@Dao
interface UserDao {

    @Query("SELECT * FROM user WHERE username = :username")
    fun getByUsername(username: String): LiveData<UserEntity>

    @Insert
    fun add(user: UserEntity)

    @Delete
    fun delete(user: UserEntity)

}