package com.gustavofao.smdevsample.service

import com.gustavofao.smdevsample.service.response.RepoResponse
import com.gustavofao.smdevsample.service.response.UserResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubService {

    @GET("search/users")
    fun queryUsersBy(@Query("q") q: String): Call<List<UserResponse>>

    @GET("users/{username}")
    fun getUser(@Path("username") username: String): Call<UserResponse>

    @GET("users/{username}/repos")
    fun getRepos(@Path("username") username: String): Call<List<RepoResponse>>

}