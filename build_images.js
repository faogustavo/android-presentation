const compress_images = require("compress-images")

// const startCompression = new Promise((resolve, reject) => {
const INPUT_DIR = "assets/*.{jpg,png,gif}"
const OUTPUT_DIT = "public/assets/"

console.log("asset processing started")
compress_images(
  INPUT_DIR,
  OUTPUT_DIT,
  { compress_force: false, statistic: true, autoupdate: true }, false,
  { jpg: { engine: 'mozjpeg', command: ['-quality', '60'] } },
  { png: { engine: 'pngquant', command: ['--quality=20-50'] } },
  { svg: { engine: 'svgo', command: '--multipass' } },
  { gif: { engine: 'gifsicle', command: ['--colors', '64', '--use-col=web'] } },
  (err, completed) => {
    if (completed === true) {
      console.log("asset processing completed")
    } else if (err) {
      console.log("asset processing rejected with error", err)
    }
  }
)
// });

