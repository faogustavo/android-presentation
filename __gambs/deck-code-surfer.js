"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _codeSurfer = require("code-surfer");

var _codeSurfer2 = _interopRequireDefault(_codeSurfer);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _mdxDeck = require("mdx-deck");

var _styledComponents = require("styled-components");

var _memoizeOne = require("memoize-one");

var _memoizeOne2 = _interopRequireDefault(_memoizeOne);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Notes = function Notes(_ref) {
  var notes = _ref.notes;
  return !notes || typeof notes === "string" ? _react2.default.createElement(
    "p",
    { style: { height: "50px" } },
    notes || "\xA0"
  ) : notes();
};

var Title = function Title(_ref2) {
  var title = _ref2.title;
  return typeof title === "string" ? _react2.default.createElement(
    "h1",
    null,
    title
  ) : title();
};

var InnerCodeSurfer = function (_React$Component) {
  _inherits(InnerCodeSurfer, _React$Component);

  function InnerCodeSurfer(props) {
    _classCallCheck(this, InnerCodeSurfer);

    var _this = _possibleConstructorReturn(this, (InnerCodeSurfer.__proto__ || Object.getPrototypeOf(InnerCodeSurfer)).call(this, props));

    _this.parseSteps = (0, _memoizeOne2.default)(function (steps, notes) {
      if (!steps) {
        return [{ notes: notes }];
      }

      if (typeof steps === "string") {
        return steps.trim().split("\n").map(function (stepAndNoteString) {
          var _stepAndNoteString$sp = stepAndNoteString.split("> "),
              _stepAndNoteString$sp2 = _slicedToArray(_stepAndNoteString$sp, 2),
              step = _stepAndNoteString$sp2[0],
              notes = _stepAndNoteString$sp2[1];

          return {
            step: step,
            notes: notes
          };
        });
      }

      return steps.map(function (_ref3) {
        var notes = _ref3.notes,
            title = _ref3.title,
            step = _objectWithoutProperties(_ref3, ["notes", "title"]);

        return { step: step, notes: notes, title: title };
      });
    });
    var _props$deck = props.deck,
        update = _props$deck.update,
        index = _props$deck.index;

    var parsedSteps = _this.parseSteps(props.steps);
    var maxStep = parsedSteps.length - 1;
    update(_mdxDeck.updaters.setSteps(index, maxStep));
    return _this;
  }

  _createClass(InnerCodeSurfer, [{
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      // console.log(nextProps.deck.id, nextProps.deck.active);
      return nextProps.deck.active;
    }
  }, {
    key: "render",
    value: function render() {
      var _props = this.props,
          code = _props.code,
          steps = _props.steps,
          title = _props.title,
          notes = _props.notes,
          theme = _props.theme,
          prismTheme = _props.prismTheme,
          showNumbers = _props.showNumbers,
          rest = _objectWithoutProperties(_props, ["code", "steps", "title", "notes", "theme", "prismTheme", "showNumbers"]);

      var stepIndex = this.props.deck.step;
      var mdxDeckTheme = theme;
      prismTheme = prismTheme || mdxDeckTheme.codeSurfer;
      showNumbers = showNumbers || prismTheme && prismTheme.showNumbers;
      var stepsWithNotes = this.parseSteps(steps, notes);

      var current = stepsWithNotes[stepIndex >= stepsWithNotes.length ? 0 : stepIndex];
      var currentStep = current && current.step;
      var currentTitle = current && current.title || title;
      var currentNotes = current && current.notes;
      var anyNotes = stepsWithNotes.some(function (s) {
        return s.notes;
      });

      return _react2.default.createElement(
        "div",
        {
          style: {
            height: "100vh",
            width: "100vw",
            background: prismTheme && prismTheme.plain.backgroundColor,
            color: prismTheme && prismTheme.plain.color,
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
          }
        },
        _react2.default.createElement(
          "div",
          {
            style: {
              height: "100vh",
              display: "flex",
              flexDirection: "column",
              justifyContent: "center"
            }
          },
          currentTitle && _react2.default.createElement(Title, { title: currentTitle }),
          _react2.default.createElement(
            "div",
            {
              style: {
                flexGrow: 1,
                display: "flex",
                alignItems: "center",
                overflow: "hidden"
              },
              key: "code"
            },
            _react2.default.createElement(_codeSurfer2.default, _extends({}, rest, {
              code: code,
              showNumbers: showNumbers,
              step: currentStep,
              theme: prismTheme,
              monospace: mdxDeckTheme && mdxDeckTheme.monospace
            }))
          ),
          anyNotes && _react2.default.createElement(Notes, { notes: currentNotes }),
          _react2.default.createElement("div", { style: { height: "35px" } })
        )
      );
    }
  }]);

  return InnerCodeSurfer;
}(_react2.default.Component);

// Things I need to do to avoid props name collisions


var EnhancedCodeSurfer = (0, _mdxDeck.withDeck)((0, _styledComponents.withTheme)(InnerCodeSurfer));

exports.default = function (_ref4) {
  var theme = _ref4.theme,
      rest = _objectWithoutProperties(_ref4, ["theme"]);

  return _react2.default.createElement(EnhancedCodeSurfer, _extends({}, rest, { prismTheme: theme }));
};