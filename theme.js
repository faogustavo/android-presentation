import { future as theme } from 'mdx-deck/themes'

import duotoneLight from "prism-react-renderer/themes/duotoneLight"

export const colors = {
  background: '#3ab7f4'
}

export const CodeTheme = {
  ...duotoneLight,
}

const deckTheme = {
  ...theme,
  font: 'Roboto, sans-serif',
  colors: {
    ...theme.colors,
    background: colors.background
  }
}

export default deckTheme
