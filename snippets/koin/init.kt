class App : Application() {

    val koinModules by lazy {
        listOf(ServiceModule, DatabaseModule)
    }

    override fun onCreate() {
        super.onCreate()
        startKoin(this, koinModules)
    }

}