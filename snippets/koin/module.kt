val DatabaseModule = module {
  single<AppDatabase> {
    Room.databaseBuilder(
      get(),
      AppDatabase::class.java,
      "app-database"
    ).build()
  }
  single<UserDao> {
    val database: AppDatabase = get()
    database.getUserDao()
  }
  single<RepoDao> {
    val database: AppDatabase = get()
    database.getRepoDao()
  }
}

val ServiceModule = module {
  single<Gson> {
    GsonBuilder()
      .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
      .create()
  }
  single<Retrofit> {
    Retrofit.Builder()
      .baseUrl("https://api.github.com")
      .addConverterFactory(
        GsonConverterFactory.create(get())
      ).build()
  }
  single<GithubService> {
    val retrofit: Retrofit = get()
    retrofit.create(GithubService::class.java)
  }
}