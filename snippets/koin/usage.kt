class KoinFragment : Fragment() {

    private lateinit var binding: KoinFragmentBinding

    val userDao: UserDao by inject()
    val repoDao: RepoDao = get()

    override fun onCreateView(
      inflater: LayoutInflater,
      container: ViewGroup?,
      savedInstanceState: Bundle?
    ): View? {
        return KoinFragmentBinding.inflate(
          inflater,
          container,
          false
        ).apply {
            binding = this

            // All other UI logic
        }.root
    }

}