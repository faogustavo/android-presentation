// Var for variables and val for constants
var selectedLetter: Char = 'a'
selectedLetter = 'b'

val PI: Double = 3.1415

// Enjoy the type inference
var selectedLetter = 'a'
val PI = 3.1415

// Use & Abuse of Null Safety!
val bob: User? = database.getUser('user_id')
bob?.department?.head?.name // Use this
bob!!.department!!.head!!.name!! // Instead of this

// Avoid long lists of getters & setters
data class Client (
  val id: Long,
  val createdAt: Date,
  val updatedAt: Date,
  val active: Boolean,
  val name: String,
  val properties: List<Property>
)

// Extend classes to add your logic
fun String.normalizeString(
    normalizeCase: Boolean = true,
    upperCaseFirstLetter: Boolean = true
): String {
    // normalize string
}

"LoReM IpSuM DolOr SIt aMET"
    .normalizeString()

// Avoid repetition
// Instead of
binding.toolbar.title.text = "AppTitle"
binding.toolbar.subtitle.hide()
binding.toolbar.backButton.setOnClickListener { onBackPressed() }

// do
binding.toolbar.apply {
    title.text = "AppTitle"
    subtitle.hide()
    backButton.setOnClickListener { onBackPressed() }
}