Picasso.get()
  .load("http://lorem.ipsum/img.png")
  .into(imageView)

fun ImageView.loadImage(
    src: String,
    @DrawableRes
    placeHolder: Int? = null,
    @DrawableRes
    error: Int? = null
) {
    var picasso = Picasso.get()
            .load(src)

    placeHolder?.let {
        picasso = picasso.placeholder(it)
    }

    error?.let {
        picasso = picasso.error(it)
    }

    picasso.into(this)
}

imaveView.loadImage("http://lorem.ipsum/img.png")

imaveView.loadImage(
  "http://lorem.ipsum/img.png",
  R.drawable.progress_indicator,
  R.drawable.error_indicator
)