@Dao
interface UserDao {

    @Query("SELECT * FROM user WHERE username = :username")
    fun getByUsername(username: String): LiveData<UserEntity>

    @Insert
    fun add(user: UserEntity)

    @Delete
    fun delete(user: UserEntity)

}

@Dao
interface RepoDao {

    @Query(
        "SELECT * FROM repository " +
        "INNER JOIN user ON user.id = repository.ownerId "+
        "WHERE user.username = :username"
    )
    fun getAll(username: String): LiveData<List<RepoEntity>>

    @Insert
    fun add(repo: RepoEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addAll(repos: List<RepoEntity>)

    @Delete
    fun delete(repos: List<RepoEntity>)

}