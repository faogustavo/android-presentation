@Entity(
    tableName = "user",
    primaryKeys = arrayOf("id")
)
data class UserEntity(
    val id: Long,
    val username: String,
    val avatarUrl: String,
    val name: String?,
    val company: String?,
    val location: String?,
    val createdAt: Date?
)

@Entity(
    tableName = "repository",
    primaryKeys = arrayOf("id"),
    foreignKeys = arrayOf(
        ForeignKey(
            entity = UserEntity::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("ownerId"),
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        )
    )
)
data class RepoEntity(
    val id: Long,
    val name: String,
    val fullName: String,
    val description: String?,
    val isFork: Boolean,
    val ownerId: Long
)