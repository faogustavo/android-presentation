data class RepoResponse(
    val id: Long,
    val name: String,
    @SerializedName("full_name")
    val fullName: String,
    val description: String?,
    @SerializedName("fork")
    val isFork: Boolean,
    val owner: UserResponse
)

data class UserResponse(
    val id: Long,
    val username: String,
    @SerializedName("avatar_url")
    val avatarUrl: String,
    val name: String?,
    val company: String?,
    val location: String?,
    @SerializedName("created_at")
    val createdAt: Date?
)

fun RepoResponse.toRepoEntity() = RepoEntity(
    id, name, fullName, description, isFork, owner.id
)

fun UserResponse.toUserEntity() = UserEntity(
    id, username, avatarUrl, name, company, location, createdAt
)