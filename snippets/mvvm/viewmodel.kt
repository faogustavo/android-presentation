class GithubViewModel
  private val githubRepository: GithubRepository
) : ViewModel() {

  private val _username: MutableLiveData<String> =
      MutableLiveData()
  private val _tryAgain: MutableLiveData<Int> =
      MutableLiveData<Int>().apply { value = 0 }

  private val _user: LiveData<Resource<UserEntity>> =
    Transformations.switchMap(_username) { username ->
      Transformations.switchMap(_tryAgain) {
        githubRepository.getUser(username)
      }
    }

  private val _repositories: LiveData<Resource<List<RepoEntity>>> =
    Transformations.switchMap(_user) { user ->
      Transformations.switchMap(_tryAgain) {
        if (user.isSuccess()) {
          user.data?.let {
            githubRepository.getRepos(it.username)
          } ?: MutableLiveData()
        } else {
          MutableLiveData()
        }
      }
    }

  val user: LiveData<Resource<UserEntity>>
      get() = _user

  val repositories: LiveData<Resource<List<RepoEntity>>>
      get() = _repositories

  fun setUsername(username: String) {
    _username.postValue(username)
  }

  fun tryAgain() {
    _tryAgain.postValue((_tryAgain.value ?: 0).inc())
  }
}