interface GithubService {

    @GET("users/{username}")
    fun getUser(@Path("username") username: String):
        Call<UserResponse>

    @GET("users/{username}/repos")
    fun getRepos(@Path("username") username: String):
        Call<List<RepoResponse>>

}