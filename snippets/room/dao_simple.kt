@Dao
interface UserDao {

    @Query("SELECT * FROM user")
    fun getAll(): List<UserEntity>

    @Insert
    fun add(user: UserEntity)

    @Update
    fun update(user: UserEntity)

    @Delete
    fun delete(user: UserEntity)

    @Query("DELETE FROM user")
    fun deleteAll()

}

@Dao
interface RepoDao {

    @Query("SELECT * FROM repository")
    fun getAll(): List<RepoEntity>

    @Insert
    fun add(repo: RepoEntity)

    @Update
    fun update(repo: RepoEntity)

    @Delete
    fun delete(repo: RepoEntity)

    @Query("DELETE FROM repository")
    fun deleteAll()

}