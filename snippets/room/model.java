public class UserModel {

    @Embedded
    public UserEntity user;

    @Relation(
        entity = RepoEntity.class,
        entityColumn = "ownerId",
        parentColumn = "id"
    )
    public List<RepoEntity> repositories;

}
