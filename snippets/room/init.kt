@Database(
    entities = arrayOf(
        UserEntity::class,
        RepoEntity::class
    ),
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getUserDao(): UserDao

    abstract fun getRepoDao(): RepoDao

}

fun database(): AppDatabase =
    Room.databaseBuilder(
        context,
        AppDatabase::class.java,
        "app-database"
    ).build()

fun userDao() =
    database().getUserDao()

fun repoDao() =
    database().getRepoDao()