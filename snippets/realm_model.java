// Define your model class by extending RealmObject
public class Dog extends RealmObject {

    private String name;
    private int age;

    @LinkingObjects("dogs")
    private final RealmResults<Person> owner = null;

    public Dog() {}
    // ... Generated getters and setters ...
}

public class Person extends RealmObject {

    @PrimaryKey
    private long id;

    private String name;
    private RealmList<Dog> dogs;

    public Person() {}
    // ... Generated getters and setters ...
}