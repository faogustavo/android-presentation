getGithubService().queryUsersBy(q)
  .enqueue(object : Callback<List<UserResponse>> {
    override fun onResponse(
      call: Call<List<UserResponse>>?,
      response: Response<List<UserResponse>>?
    ) {
      val data: List<UserResponse>
      if (response?.isSuccessful == true) {
          // Success
          data = response.body() ?: emptyList()
      } else {
          // Error from request (server side)
          data = emptyList()
      }
    }

    override fun onFailure(
      call: Call<List<UserResponse>>?,
      t: Throwable?
    ) {
      // Error from client side
      // Like IO, disconnected
    }
  })