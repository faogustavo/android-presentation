interface GithubService {

    // https://api.github.com/search/users?q=faogustavo
    @GET("search/users")
    fun queryUsersBy(
      @Query("q") q: String
    ): Call<List<UserResponse>>

    // https://api.github.com/users/faogustavo
    @GET("users/{username}")
    fun getUser(
      @Path("username") username: String
    ): Call<UserResponse>

    // https://api.github.com/users/faogustavo/repos
    @GET("users/{username}/repos")
    fun getRepos(
      @Path("username") username: String
    ): Call<List<RepoResponse>>

}

fun getGithubService(): GithubService {
  val retrofit = Retrofit.Builder()
    .baseUrl("https://api.github.com/")
    .addConverterFactory(GsonConverterFactory.create()))
    .build()

  return retrofit.create(GithubService::class.java)
}
