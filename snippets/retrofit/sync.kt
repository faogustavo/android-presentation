try {
  val response = getGithubService().queryUsersBy(q).execute()
  val data: List<UserResponse>
  if (response.isSuccessful) {
    // Success
    data = response.body() ?: emptyList()
  } else {
    // Error from request (server side)
    data = emptyList()
  }
} catch (e: Throwable) {
  // Error from call (client side)
  // Like IO, disconnected
}