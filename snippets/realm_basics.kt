// Initialize Realm
// just once per application
Realm.init(context)

// Get Realm instance
val realm = Realm.getDefaultInstance()

//
// CREATE
//

// Start a transaction
realm.beginTransation()

// Create a new data
val puppy = Dog().apply {
    name = "Loki"
    age = 1
}

// And save it on the database
realm.copyToRealm(puppy)

// Or create a new instance direct and then set the valures
val person = realm.createObject(User::class.java).apply {
    name = "Gustavo"
    dogs.add(puppy)
}

// Save the changes
realm.commitTransaction()

//
// READ
//

// Query by Dog entity
val allDogs = realm.where(Dog::class.java).findAll();

// Query with filter
val puppies = realm.where(Dog::class.java)
    .lessThan("age", 2)
    .findAll()

// Query single person
val person = realm.where(Person::class.java)
    .equalTo("uuid", "random-uuid")
    .findFirst()

//
// UPDATE
//

puppy.age = 2

realm.beginTransation()
realm.copyToRealmOrUpdate(puppy)
realm.commitTransaction()

//
// DELETE
//

realm.beginTransation()
realm.where(Person::class.java)
    .equalTo("uuid", uuid)
    .findFirst()?.
    .deleteFromRealm()
realm.commitTransaction()

// Remove boilerplate with kotlin
fun Realm.onTransaction(block: Realm.() -> Unit) {
    beginTransaction()
    block()
    commitTransaction()
}

fun savePerson(person: Person) {
    realm.onTransaction {
        copyToRealmOrUpdate(person)
    }
}

fun <T : RealmModel> Realm.findAll(
    clazz: Class<T>,
    orderBy: String? = null,
    limit: Long? = null
): RealmResults<T> {
    var query = where(clazz)

    orderBy?.let {
        query = query.sort(it)
    }

    limit?.let {
        query = query.limit(it)
    }

    return query.findAll()
}

fun getAllPeople() = realm.findAll(Person::class.java)

fun <T : RealmModel> Realm.findFirst(
    clazz: Class<T>,
    fieldName: String? = null,
    fieldValue: String? = null
): T? {
    var query = where(clazz)

    fieldName?.let {
        query = query.equalTo(fieldName, fieldValue)
    }

    return query.findFirst()
}

getPerson(uuid: String) = realm.findFirst(Person::class.java, "uuid", uuid)