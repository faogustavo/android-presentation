class Resource<T>(
  val data: T?,
  val errorMessage: String?,
  val status: Status
) {
  enum class Status {
    LOADING,
    SUCCESS,
    ERROR
  }

  companion object {
    fun <T> loading() =
      Resource<T>(null, null, Status.LOADING)

    fun <T> success(data: T? = null) =
      Resource<T>(data, null, Status.SUCCESS)

    fun <T> error(message: String? = null) =
      Resource<T>(null, message, Status.ERROR)
  }

  fun isSuccess() = status == Status.SUCCESS

  fun isError() = status == Status.ERROR

  fun loading() = status == Status.LOADING
}