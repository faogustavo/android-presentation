class GithubFragment : Fragment() {

  private val adapter = ReposAdapter()

  private val githubService: GithubService = get()
  private val repositories =
     MutableLiveData<Resource<List<RepoResponse>>>()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    repositories.observe(this, Observer {
      when(it?.status) {
        Resource.Status.LOADING -> {
          // Show progress indicator
        }
        Resource.Status.ERROR -> {
          // Show error message
        }
        Resource.Status.SUCCESS -> {
          adapter.setData(it.data ?: emptyList())
          // Do another logic
        }
      }
    })
  }

  fun executeRequest() {
    repositories.postValue(Resource.loading())
    githubService.getRepos("faogustavo")
      .enqueue(object: Callback<List<RepoResponse>> {
        override fun onFailure(
          call: Call<List<RepoResponse>>?,
          t: Throwable?
        ) {
          repositories.postValue(
            Resource.error(t?.message)
          )
        }

        override fun onResponse(
          call: Call<List<RepoResponse>>?,
          response: Response<List<RepoResponse>>?
        ) {
          if (response?.isSuccessful == true) {
            repositories.postValue(
              Resource.success(response.body())
            )
          } else {
            repositories.postValue(
              Resource.error("Error decoding body")
            )
          }
        }
      })
  }

}