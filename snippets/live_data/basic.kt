class GithubFragment : Fragment() {

  private val adapter = ReposAdapter()

  private val githubService: GithubService = get()
  private val repositories = MutableLiveData<List<RepoResponse>>()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    repositories.observe(this, Observer {
      it?.let {
        adapter.setData(it)
        // Do another logic
      }
    })
  }

  fun executeRequest() {
    githubService.getRepos("faogustavo")
      .enqueue(object: Callback<List<RepoResponse>> {
        override fun onFailure(
          call: Call<List<RepoResponse>>?,
          t: Throwable?
        ) {
          // and this error?
        }

        override fun onResponse(
            call: Call<List<RepoResponse>>?,
            response: Response<List<RepoResponse>>?
        ) {
          if (response?.isSuccessful == true) {
            // Instead of
            adapter.setData(response.body() ?: emptyList())

            // Do this
            repositories.postValue(response.body())
          } else {
            // Where to show the error?
          }
        }
      })
  }

}