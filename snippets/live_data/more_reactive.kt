private val githubRepository: GithubRepository = get()
private val username = MutableLiveData<String>()

private val userInfo: LiveData<Resource<UserResponse>> =
  Transformations.switchMap(username) {
    githubRepository.getUser(it)
  }

private val repositories: LiveData<Resource<List<RepoResponse>>> =
  Transformations.switchMap(username) {
    githubRepository.getRepos(it)
  }

private val repositories: LiveData<Resource<List<RepoResponse>>> =
  Transformations.switchMap(userInfo) {
    if (it.isSuccess() && it.data != null) {
      githubRepository.getRepos(it.data.username)
    } else {
      MutableLiveData()
    }
  }

private val tryAgainLoadUser =
  MutableLiveData<Int>().apply { value = 0 }

private val userInfo: LiveData<Resource<UserResponse>> =
  Transformations.switchMap(tryAgainLoadUser) {
    Transformations.switchMap(username) {
      githubRepository.getUser(it)
    }
  }

fun tryAgainClick() {
  tryAgainLoadUser.postValue((tryAgainLoadUser.value ?: 0).inc())
}