// Use & Abuse of Null Safety!
val bob: User? = database.getUser('user_id')
bob?.department?.head?.name // Use this
bob!!.department!!.head!!.name!! // Instead of this

// Enjoy the type inference
val name = "Some String"
val someInt = 123

// Make your methods as small
fun sum(a: Int, b: Int): Int {
    return a + B
}

fun sum(a: Int, b: Int) = a + b

// Data classes to avoid getters and setters
data class Client (
  val id: Long,
  val createdAt: Date,
  val updatedAt: Date,
  val active: Boolean,
  val name: String,
  val properties: List<Property>
)

// Use & Create Extensions!
fun String.normalizeString(
    normalizeCase: Boolean = true,
    upperCaseFirstLetter: Boolean = true
): String {
    // normalize string
}

"LoReM IpSuM DolOr SIt aMET"
    .normalizeString()

// Lambdas without importing ANYTHING!
val simpleLambda: (String) -> Unit = { parameter ->
    println("simpleFun(\"$parameter\")")
}
simpleLambda("Hello World")

// Use functional methods
events //:List<String>
  .filter { it.startsWith("a", true) }
  .sortedBy { it }
  .map { it.normalizeString() }
  .forEach { adapter.addItem(it) }

// Create alias
typealias ASecond = A.Inner.Second
typealias SimpleFun = () -> Unit
typealias GenericCallback<I, O> = (I) -> O

// Avoid repetition
// Instead of
binding.toolbar.title.text = "AppTitle"
binding.toolbar.subtitle.hide()
binding.toolbar.backButton.setOnClickListener { onBackPressed() }

// do
binding.toolbar.apply {
    title.text = "AppTitle"
    subtitle.hide()
    backButton.setOnClickListener { onBackPressed() }
}