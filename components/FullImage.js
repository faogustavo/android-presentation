import React from 'react'
import styled from 'styled-components'

import Flex from './Flex'
import Box from './Box'

import { colors } from '../theme'

export default ({ src, children, header, headerCss }) => {
  const View = styled.div([], {
    width: '100vw',
    height: '100vh',
  });

  return (
    <Flex css={{
      width: '100vw',
      height: '100vh',
      flexDirection: 'column',
      alignItems: 'flex-end',
      justifyContent: 'flex-end',
      position: 'relative',
      flexGrow: 1,
      backgroundImage: `url("${src}")`,
      backgroundPosition: 'center',
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
    }}>
      <Box css={{
        marginBottom: '32px',
        paddingLeft: '32px',
        paddingRight: '32px',
        minWidth: '100vw',
        position: 'absolute',
        top: 0
      }}>
        <h2 style={{
          ...headerCss,
          color: 'black',
          textShadow: '-1px -1px 0 white, 1px -1px 0 white, -1px 1px 0 white, 1px 1px 0 white',
          fontSize: '48pt'
        }}>{header}</h2>
      </Box>
      {children && (
        <Box css={{
          background: colors.background,
          marginBottom: '32px',
          paddingLeft: '32px',
          paddingRight: '32px',
          minWidth: '40vw',
        }}>
          <h2>{children}</h2>
        </Box>
      )}
    </Flex>
  );
}