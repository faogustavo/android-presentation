import React from 'react'

const Small = ({ children, quantity = 1, paddingLeft, paddingRight }) => {
  let style = {}
  
  if (paddingLeft) {
    style.paddingLeft = paddingLeft
  }

  if (paddingRight) {
    style.paddingRight = paddingRight
  }

  return (
    <small style={style}>
      {quantity == 1 ? children : (<Small quantity={quantity - 1}>{children}</Small>)}
    </small>
  )
}

export default Small