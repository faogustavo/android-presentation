import React from "react";
import { withDeck } from "mdx-deck";
import { CodeSurfer } from "mdx-deck-code-surfer";
import { CodeTheme } from "../theme";

export default withDeck(
  ({ title, lang = "java", snippetName, steps = true, stepsFileName = `${snippetName}.json` }) => (
    <CodeSurfer
      title={title}
      theme={CodeTheme}
      code={require(`!raw-loader!../snippets/${snippetName}`)}
      steps={steps ? require(`../snippets/${stepsFileName}`) : null}
      lang={lang}
      showNumbers
    />
  )
);
