import React from 'react'

export default ({ children, ...otherProps }) => (
  <ol style={{ textAlign: 'left' }} {...otherProps} >{children}</ol>
)